import React, { Component } from 'react';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputText: '',
      outputText: ''
    };
  }

  onChangeText = event => {
    const strings = event.target.value.split('\n');
    const stringsWithCharacters = strings.map(string => new Set(string.split(/(?!$)/u)));
    const uniqueCharactersAmount = stringsWithCharacters.reduce((indexes, item) => [...indexes, item.size], []);
    const stringsToOutput = strings.filter((item, index) => {
          return item.length === uniqueCharactersAmount[index] + 1;
        }, []);

    const finalOutputString = stringsToOutput.reduce((string, current) => {
      return string+= `${current}\n`;
    }, '');

    this.setState({
      inputText: event.target.value,
      outputText: finalOutputString
    });
  };

  render() {
    return (
        <div className="App">
          <header className="App-header">
            <h3>Repeating characters text</h3>
          </header>

          <div className="container">
            <div className="row">
              <div className="row-item-header">Input</div>
              <div className="row-item-header">Output</div>
            </div>

            <div className="row">
            <textarea className="row-item"
                      value={this.state.inputText}
                      onChange={this.onChangeText} />
              <textarea className="row-item"
                        value={this.state.outputText}
                        defaultValue={this.state.outputText} />
            </div>
          </div>

        </div>
    );
  }
}

export default App;
